# Kaggler競賽 - Predict Future Sales

Kaggle競賽案例學習 - [Feature engineering, xgboost](https://www.kaggle.com/dlarionov/feature-engineering-xgboost#Part-1,-perfect-features)

## Week3

![](W3/rank.png)


## Week4

![](W4/score.png)

* Task1
  * 刪除多餘的期別
  * 名次上升13

![](W4/rank_2-1.png)

* Task2
  * 增加期別
  * 名次上升18

![](W4/rank_2-2.png)

## Week5

* 1.請參考Notebook最後跑出來的Feature Important，能夠找到哪些有用的資訊?
  * 月份是很重要的變數，可能有週期性，可以嘗試做季度或去年同期類變數
  * 重要變數中很多與品項相關，可能特定品項特別容易/不容易預測，可能可以嘗試將品項分群觀察預測表現

![](W5/feature_importance.png)

* 2.請找出一個目前模型預測較不準的產品，這些產品有什麼特性?
  * 商店：9/20 號商店預測特別不準
  * 品項：item_category_id 71/42/79特別不準

## Week6

* 1.本模型是否會隨著銷售量越低，item_category_id猜不準的比例越高？
  * 銷售量越高，預測不準確的比例越高

![](W6/Q1_1.png)
![](W6/Q1_2.png)

* 2.猜不準的item_category_id前三名為？ 這三名是否因為銷售量較差，所以猜不準？
  * 55, 40, 37
  * 檢視前32個月份銷售資料，藍色線為預測不準確比例最高前三名，黃色線為預測不準確比例最低三名。發現藍色線銷售量明顯比黃色線高非常多，顯示預測不準確並非因為銷售量較低

![](W6/Q2.png)

* 3.本模型是否會隨著銷售量越低，city_code猜不準的比例越高？
  * 從第33個月份資料無法明確看出預測準確度與銷售量的關聯性

![](W6/Q3_1.png)
![](W6/Q3_2.png)

  * 檢視前32個月份銷售資料，藍色線為預測不準確比例最高前三名，黃色線為預測不準確比例最低三名。藍色線銷售量確實比黃色線銷售量高，所以可能與銷售量有一定的關係，需要再進一步檢視。預測最不準確的city13，除了銷售量高之外，銷售量的變動非常劇烈，有可能預測準確度與銷售變動幅度有關係

![](W6/Q3_3.png)

  







